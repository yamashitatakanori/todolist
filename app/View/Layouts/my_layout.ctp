<?php
	$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('styles.css');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
	<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css" >
</head>
<body>
	<div id="container">
		<div id="header">
			<h1><?php echo $this->Html->link('ToDo App', '/'); ?></h1>
		</div>
	<div id="content">
			<?php echo $this->Session->flash(); ?>
			<?php echo $this->fetch('content'); ?>
	</div>	
	<?php echo $this->element('sql_dump'); ?>
	
<script>
	$(function() {
		setTimeout(function() {
			$('#flashMessage').fadeOut("slow");
		}, 800);
	});

	$(function() {
		$.datepicker.setDefaults({
			showOn: "both",
			buttonImage: '/mytodo/app/webroot/img/calender.png',
			buttonText: "カレンダーから選択",
			buttonImageOnly: true,
			minDate: '0y',
			dateFormat: 'yy-mm-dd(D)'
		});
	
		$('#expected_date').datepicker();
		$('#completion_date').datepicker();
	});	
</script>
</body>
</html>
