<h2>タスク登録</h2>
<!-- 登録フォーム -->
<?php
echo $this->Form->create('Post', array('action'=>'add',
									   'onsubmit'=>'return confirm("登録してもよろしいですか？")'));
echo $this->Form->input('title',array('label' => array('text' => 'タイトル')));
echo $this->Form->input('body', array('rows'=>3,'label' => array('text' => 'タスクの内容')));
echo $this->Form->input('expected_date',array('style'=>'width:250px','label' => array('text' => '予定日'),
						"type" => "text", "id" => "expected_date"));
echo $this->Form->input('completion_date',array('style'=>'width:250px','label' => array('text' => '完了日'),
						"type" => "text", "id" => "completion_date"));
$prioritylist = array(" "=>' ', "低" => '低' , "中" =>  '中', "高" => '高');
echo $this->Form->input('priority',array('style'=>'width:50px','label' => array('text' => '優先順位'),
	'type' => 'select',
	'options' => $prioritylist,
	'selected' => " "));
echo $this->Form->end('登録');
?>

