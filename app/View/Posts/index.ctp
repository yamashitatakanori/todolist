<br>
<br>
<br>
<p class='add'><?php echo $this->Html->link('　タスク追加　', array('controller'=>'posts','action'=>'add'),
															array('class'=>'button')); ?>
</p>
<table>
<br>
<br>
<caption>未完了タスク一覧</caption>
<tr>
	<th>完了</th>
	<th>タイトル</th>
	<th>予定日</th>
	<th>完了日</th>
	<th>優先度</th>
	<th>操作</th>
</tr>

<!-- 未完了タスク -->
<?php foreach ($yet_posts as $post) : ?>
<tr>
	<td width="5%">
		<input type=checkbox class="checkbox" onclick="location.href='/mytodo/posts/done/<?php 
		echo h($post['Post']['id']); echo h("/'"); ?>">
	</td>
	<td width="35%" class="todo_title">
		<?php echo $this->Html->link($post['Post']['title'],'/posts/edit/'.$post['Post']['id']);?>
	</td>
	<td><?php echo h($post['Post']['expected_date']); ?> </td>
	<td><?php echo h($post['Post']['completion_date']); ?> </td>
	<td width="8%"><?php echo h($post['Post']['priority']); ?> </td>
	<td width="6%">
		<?php
			echo $this->Html->link('削除',array('action'=>'delete', $post['Post']['id']),
								   array('class'=>'delete grad2','confirm'=>'削除してもよろしいですか?'));
		?>
	</td>
</tr>
<?php endforeach; ?>
</table>
<br>
<!-- 未完了タスクが0件の際にメッセージを表示 -->
<p class="errormessage"><?php if (empty($yet_posts)) { echo "※未完了タスクがありません。"; } ?> </p>

<table>
<caption>完了タスク一覧</caption>
<tr>
	<th>タイトル</th>
	<th>予定日</th>
	<th>完了日</th>
	<th>優先度</th>
	<th>操作</th>
</tr>

<!-- 完了タスク -->
<?php foreach ($done_posts as $post) : ?>
<tr>
	<td><?php echo $this->Html->link($post['Post']['title'],'/posts/edit/'.$post['Post']['id']); ?> </td>
	<td><?php echo h($post['Post']['expected_date']); ?> </td>
	<td><?php echo h($post['Post']['completion_date']); ?> </td>
	<td width="8%"><?php echo h($post['Post']['priority']); ?> </td>
	<td width="6%">
		<?php
			echo $this->Html->link('削除', array('action'=>'delete', $post['Post']['id']),
								   array('class'=>'delete grad2','confirm'=>'削除してもよろしいですか?'));
		?>
	</td>
</tr>
<?php endforeach; ?>
</table>
<br>
<!-- 完了タスクが0件の際にメッセージを表示 -->
<p class="errormessage"><?php if (empty($done_posts)) { echo "※完了タスクがありません。"; } ?> </p>