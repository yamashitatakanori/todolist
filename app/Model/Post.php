<?php
class Post extends AppModel {
	//必須入力項目バリデーション
	public $validate = array(
		'title' => array(
			'rule' => 'notEmpty',
			'message' => 'タイトルが入力されていません。'
		),
		'body' => array(
			'rule' => 'notEmpty',
			'message' => 'タスク内容が入力されていません。'
		),
		'expected_date' => array(
			'rule' => 'notEmpty',
			'message' => '予定日が入力されていません。'
		),
		'completion_date' => array(
			'rule' => 'notEmpty',
			'message' => '完了日が入力されていません。'
		),
		'priority' => array(
			'rule' => 'notEmpty',
			'message' => '優先度が入力されていません。'
		),	
	);
}