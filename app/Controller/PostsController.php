<?php
class PostsController extends AppController {
	//Html・Formヘルパーを有効に
	public $helpers = array('Html' , 'Form');
	//独自作成のレイアウトをPostsController直下に適応
	public $layout = 'my_layout';
	
	//Home画面
	public function index() {
		//postsテーブルの全データ取得
		$this->set('posts', $this->Post->find('all'));
		//ページタイトル
		$this->set('title_for_layout', 'タスク一覧');
		//予定日と完了日のデータを取得
		$this->set('yet_posts',$this->Post->findAllByStatus('yet',null,'Post.expected_date ASC'));
		$this->set('done_posts',$this->Post->findAllByStatus('done',null,'Post.completion_date DESC'));
	}
	
	//sutatusをyetからdoneに変更
	public function done($id) {
		if ($this->Post->findById($id)) {
			$this->Post->id = $id;
			$this->Post->save(array('status' => 'done'));
		}
		$this->redirect('/posts');
	}
	
	//タスクの追加
	public function add() {
		
		$this->set('title_for_layout', 'タスク登録');
		
		if ($this->request->is('post')) {
			if($this->Post->save($this->request->data)) {
				$this->Session->setFlash('Success!');
				$this->redirect(array('action'=> 'index'));
			} else {
				$this->Session->setFlash('failed!');
			}
		}
	}
	
	//タスクの編集
	public function edit($id = null) {
		
		$this->set('title_for_layout', 'タスク編集・閲覧');
		
		$this->Post->id = $id;
		if($this->request->is('get')) {
			$this->request->data = $this->Post->read();
		} else {
			if($this->Post->save($this->request->data)) {
				$this->Session->setFlash('success!');
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash('failed!');
			}
		}
	}
	
	//タスクの削除
	public function delete($id) {
		
        if ($this->Post->delete($id)) {
            $this->Session->setFlash('Deleted!');
            $this->redirect(array('action'=>'index'));
        }
    
    }
}